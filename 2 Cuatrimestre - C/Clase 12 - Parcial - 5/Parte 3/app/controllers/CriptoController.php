<?php
require_once './models/Usuario.php';
include_once './utils/JwtUtil.php';
require_once './models/Cripto.php';

use Slim\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class CriptoController
{
    public function AltaCripto(Request $request, Response $response, $args)
    {
        $parametros = $request->getParsedBody();
        $data = json_decode($request->getAttribute("dataToken"));

        $nombre = strtolower($parametros['nombre']);

        $nacionalidad = strtolower($parametros['nacionalidad']);

        $precio = $parametros['precio'];

        $tipo = explode("/", $request->getUploadedFiles()["imagen"]->getClientMediaType())[1];
        $fotoUrl = $nombre . "-" . $nacionalidad . "-" . $data->id . "." . $tipo;
        $destino = "./FotosAltaCripto/" . $fotoUrl;
        $request->getUploadedFiles()["imagen"]->moveTo($destino);

        Cripto::CrearCripto($nombre, $nacionalidad, $fotoUrl, $precio);

        $payload = json_encode(array("Mensaje:" => "Criptomoneda dada de alta correctamente"));

        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(201);
    }
    public function ListarCriptos(Request $request, Response $response, $args)
    {
        $arrayCriptos = Cripto::TraerTodos();

        $payload = json_encode(array("Listado de Criptos:" => $arrayCriptos));

        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }

    public function ListarCriptosNacionalidad(Request $request, Response $response, $args)
    {
        $nacionalidad = strtolower($args["nacionalidad"]);
        $arrayCriptos = Cripto::TraerUnoNacionalidad($nacionalidad);

        $payload = json_encode(array("Listado de Criptos de nacionalidad " . $nacionalidad . ": " => $arrayCriptos));

        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
    public function ListarCriptoId(Request $request, Response $response, $args)
    {
        $id = $args["id"];
        $cripto = Cripto::TraerUnoId($id);

        $cripto ? $payload = json_encode(array("Cripto de id " . $id . ": " => $cripto)) : $payload = json_encode("No se encontro la criptomoneda con el id indicado");

        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
    }
    public function VentaCripto(Request $request, Response $response, $args)
    {
        $parametros = $request->getParsedBody();
        $data = json_decode($request->getAttribute("dataToken"));

        $idUsuario = $data->id;
        $nombreUsuario = explode("@", $data->mail)[0];
        $idCripto = $parametros['idCripto'];
        $cripto = Cripto::TraerUnoId($idCripto);
        if (!$cripto) {
            $payload = json_encode("No se encontro la criptomoneda con el id indicado");
            $response->getBody()->write($payload);
            return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
        }
        $cantidad = $parametros['cantidad'];

        $fecha = date("Y-m-d H:m:s");

        $tipo = explode("/", $request->getUploadedFiles()["imagen"]->getClientMediaType())[1];
        $fotoUrl = $cripto->nombre . "-" . $nombreUsuario . "-" . $fecha . "." . $tipo;
        $destino = "./FotosCripto/" . $fotoUrl;
        $request->getUploadedFiles()["imagen"]->moveTo($destino);

        Cripto::VentaCripto($idUsuario, $idCripto, $cantidad, $fecha, $fotoUrl);

        $response->getBody()->write(json_encode("Se vendio al cripto correctamente"));
        return $response->withHeader('Content-Type', 'application/json')->withStatus(201);
    }
    public function ListarCriptoVentaAlemanaNov(Request $request, Response $response, $args)
    {

        $arrayCriptos = Cripto::TraerVentasAlemanaNov();

        $payload = json_encode(array("Listado de ventas de la nacionalidad alemana entre el 10 y 13 de noviembre: " => $arrayCriptos));

        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }

    public function BorrarCripto(Request $request, Response $response, $args)
    {
        $idCripto = $args["id"];
        if (!Cripto::BorrarCripto($idCripto)) {
            $payload = json_encode("No se pudo borrar el ID indicado");

            $response->getBody()->write($payload);
            return $response->withHeader('Content-Type', 'application/json')->withStatus(404);

        }
        $payload = json_encode("Se pudo borrar correctamente el ID");

        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}