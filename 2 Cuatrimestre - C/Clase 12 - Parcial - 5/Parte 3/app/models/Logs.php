<?php
class Logs
{
    public $id_usuario;
    public $id_cripto;
    public $accion;
    public $fecha_accion;

    public static function CrearLog($id_usuario, $id_cripto, $accion, $fecha_accion)
    {
        $objAccesoDatos = AccesoDatos::obtenerInstancia();
        $consulta = $objAccesoDatos->prepararConsulta("INSERT INTO logs (id_usuario, id_cripto, accion, fecha_accion) VALUES (:id_usuario, :id_cripto, :accion, :fecha_accion)");
        $consulta->bindValue(':id_usuario', $id_usuario, PDO::PARAM_INT);
        $consulta->bindValue(':nacionalidad', $id_cripto, PDO::PARAM_INT);
        $consulta->bindValue(':accion', $accion, PDO::PARAM_STR);
        $consulta->bindValue(':fecha_accion', $fecha_accion);
        $consulta->execute();

        return $consulta->fetchObject('Cripto');
    }
}