<?php
use Illuminate\Contracts\Support\Responsable;

date_default_timezone_set('America/Argentina/Buenos_Aires');
// Error Handling
error_reporting(-1);
ini_set('display_errors', 1);

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Factory\AppFactory;
use Slim\Routing\RouteCollectorProxy;
use Slim\Routing\RouteContext;

require __DIR__ . '/../vendor/autoload.php';

require_once './db/AccesoDatos.php';
// require_once './middlewares/Logger.php';

require_once './controllers/UsuarioController.php';
require_once './controllers/CriptoController.php';
require_once './middlewares/AuthJWT.php';

// Load ENV
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->safeLoad();

// Instantiate App
$app = AppFactory::create();

// Add error middleware
$app->addErrorMiddleware(true, true, true);

// Add parse body
$app->addBodyParsingMiddleware();

// Routes
$app->post("/loginusuario[/]", \UsuarioController::class . ':LoginUsuario'); //Devuelve un token a un usuario regsitrado

$app->post("[/]", \UsuarioController::class . ':VerificarUsuario')->add(\AuthJWT::class . ':VerificarTokenValido'); //Verifica el token enviado

$app->group("/cripto", function (RouteCollectorProxy $group) {

    $group->post("/altacripto[/]", \CriptoController::class . ':AltaCripto')->add(\AuthJWT::class . ':VerificarAdminToken')->add(\AuthJWT::class . ':VerificarTokenValido'); //Da de alta una cripto moneda

    $group->get("/listadocriptos[/]", \CriptoController::class . ':ListarCriptos'); //Todas las criptos por parametros

    $group->get("/listarcripto/{nacionalidad}[/]", \CriptoController::class . ':ListarCriptosNacionalidad'); //Criptos por nacionalidad

    $group->get("/{id}[/]", \CriptoController::class . ':ListarCriptoId')->add(\AuthJWT::class . ':VerificarTokenValido'); //Lista una cripto por ID

});
$app->group("/venta", function (RouteCollectorProxy $group) {
    $group->post("/vendercripto[/]", \CriptoController::class . ':VentaCripto')->add(\AuthJWT::class . ':VerificarTokenValido'); //Vende una cripto
});

$app->run();