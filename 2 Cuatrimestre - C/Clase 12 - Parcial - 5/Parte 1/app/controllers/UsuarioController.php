<?php
require_once './models/Usuario.php';
include_once './utils/JwtUtil.php';

use Slim\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class UsuarioController
{
  public function LoginUsuario(Request $request, Response $response, $args)
  {
    $parametros = $request->getParsedBody();

    $mail = $parametros['mail'];
    $clave = $parametros['clave'];

    $usuario = Usuario::ObtenerUsuario($mail, $clave);

    if (!$usuario) {
      $data = "No existe el usuario";
    } else {
      $data = JwtUtil::CrearToken($usuario);
    }

    $payload = json_encode(array("Datos usuario" => $data));

    $response->getBody()->write($payload);
    return $response
      ->withHeader('Content-Type', 'application/json')->withStatus(200);
  }

  public function VerificarUsuario(Request $request, Response $response, $args)
  {
    try {
      $data = json_decode($request->getAttribute("dataToken"));
      $usuario = Usuario::ObtenerUsuario($data->mail, $data->clave);

      if (!$usuario) {
        $payload = "No coincide el usuario";
      } else {
        $payload = "El usuario es de tipo: " . $data->tipo;
      }
    } catch (Exception $err) {
      $payload = json_encode(array("Error" => $err->getMessage()));
    }

    $response->getBody()->write($payload);
    return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
  }
}