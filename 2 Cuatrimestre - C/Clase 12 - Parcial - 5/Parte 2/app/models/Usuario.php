<?php

class Usuario
{
    public $mail;
    public $tipo;
    public $clave;

    public static function ObtenerUsuario($mail, $clave)
    {
        $objAccesoDatos = AccesoDatos::obtenerInstancia();
        $consulta = $objAccesoDatos->prepararConsulta("SELECT * FROM usuarios WHERE mail = :mail AND clave = :clave");
        $consulta->bindValue(':mail', $mail, PDO::PARAM_STR);
        $consulta->bindValue(':clave', $clave, PDO::PARAM_STR);
        $consulta->execute();

        return $consulta->fetchObject('Usuario');
    }

    public static function ObtenerVentaCripto($id)
    {
        $objAccesoDatos = AccesoDatos::obtenerInstancia();
        $consulta = $objAccesoDatos->prepararConsulta("SELECT * FROM usuarios INNER JOIN venta_cripto ON venta_cripto.id_usuario = usuarios.id WHERE venta_cripto.id_cripto = :id");
        $consulta->bindValue(":id", $id);
        $consulta->execute();

        return $consulta->fetchAll(PDO::FETCH_ASSOC);
    }
}