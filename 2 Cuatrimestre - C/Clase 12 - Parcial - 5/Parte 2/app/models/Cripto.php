<?php

class Cripto
{
    public $nombre;
    public $nacionalidad;
    public $fotoURL;
    public $precio;

    public static function CrearCripto($nombre, $nacionalidad, $fotoURL, $precio)
    {
        $objAccesoDatos = AccesoDatos::obtenerInstancia();
        $consulta = $objAccesoDatos->prepararConsulta("INSERT INTO criptos (nombre, nacionalidad, fotoURL, precio) VALUES (:nombre, :nacionalidad, :fotoURL, :precio)");
        $consulta->bindValue(':nombre', $nombre, PDO::PARAM_STR);
        $consulta->bindValue(':nacionalidad', $nacionalidad, PDO::PARAM_STR);
        $consulta->bindValue(':fotoURL', $fotoURL, PDO::PARAM_STR);
        $consulta->bindValue(':precio', $precio, PDO::PARAM_INT);
        $consulta->execute();

        return $consulta->fetchObject('Cripto');
    }

    public static function TraerTodos()
    {
        $objAccesoDatos = AccesoDatos::obtenerInstancia();
        $consulta = $objAccesoDatos->prepararConsulta("SELECT nombre, nacionalidad, precio FROM criptos");
        $consulta->execute();

        return $consulta->fetchAll(PDO::FETCH_ASSOC);
    }
    public static function TraerUnoNacionalidad($nacionalidad)
    {
        $objAccesoDatos = AccesoDatos::obtenerInstancia();
        $consulta = $objAccesoDatos->prepararConsulta("SELECT nombre, nacionalidad, precio FROM criptos WHERE nacionalidad = :nacionalidad");
        $consulta->bindValue(":nacionalidad", $nacionalidad);
        $consulta->execute();

        return $consulta->fetchAll(PDO::FETCH_ASSOC);
    }
    public static function TraerUnoId($id)
    {
        $objAccesoDatos = AccesoDatos::obtenerInstancia();
        $consulta = $objAccesoDatos->prepararConsulta("SELECT nombre, nacionalidad, precio, fotoURL FROM criptos WHERE id = :id");
        $consulta->bindValue(":id", $id);
        $consulta->execute();

        return $consulta->fetchObject("Cripto");

    }
    public static function VentaCripto($idUsuario, $idCripto, $cantidad, $fecha, $fotoURL)
    {

        $objAccesoDatos = AccesoDatos::obtenerInstancia();
        $consulta = $objAccesoDatos->prepararConsulta("INSERT INTO venta_cripto (id_usuario, id_cripto, cantidad, fecha, fotoURL) VALUES (:idUsuario, :idCripto, :cantidad, :fecha, :fotoURL)");
        $consulta->bindValue(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $consulta->bindValue(':idCripto', $idCripto, PDO::PARAM_INT);
        $consulta->bindValue(':cantidad', $cantidad, PDO::PARAM_INT);
        $consulta->bindValue(':fecha', $fecha);
        $consulta->bindValue(':fotoURL', $fotoURL, PDO::PARAM_STR);
        $consulta->execute();
    }

    public static function TraerVentasAlemanaNov()
    {

        $objAccesoDatos = AccesoDatos::obtenerInstancia();
        $consulta = $objAccesoDatos->prepararConsulta("SELECT * FROM venta_cripto INNER JOIN criptos ON venta_cripto.id_cripto = criptos.id WHERE venta_cripto.fecha > '2022-11-10 00:00:00' AND  venta_cripto.fecha < '2022-11-15 00:00:00'");
        $consulta->execute();

        return $consulta->fetchAll(PDO::FETCH_ASSOC);
    }
    public static function TraerVentasNombre($id)
    {

        $objAccesoDatos = AccesoDatos::obtenerInstancia();
        $consulta = $objAccesoDatos->prepararConsulta("SELECT * FROM venta_cripto INNER JOIN criptos ON venta_cripto.id_cripto = :id");
        $consulta->bindValue(":id", $id);
        $consulta->execute();

        return $consulta->fetchAll(PDO::FETCH_ASSOC);
    }
    public static function TraerUnoIdNombre($nombre)
    {
        $objAccesoDatos = AccesoDatos::obtenerInstancia();
        $consulta = $objAccesoDatos->prepararConsulta("SELECT id FROM criptos WHERE nombre = :nombre");
        $consulta->bindValue(":nombre", $nombre);
        $consulta->execute();

        return $consulta->fetch();

    }
}