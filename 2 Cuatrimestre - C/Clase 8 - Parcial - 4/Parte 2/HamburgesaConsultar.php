<?php
include_once "./Hamburgesa.php";

if (isset($_POST["nombre"]) && isset($_POST["tipo"])) {
    $nombre = $_POST["nombre"];
    $tipo = strtolower($_POST["tipo"]);
    if ($tipo != "simple" && $tipo != "doble") {
        throw new Error("Tipo invalido.", 400);
    }
    $arrayHamburgesa = Hamburgesa::CargarHamburgesaArrayJSON();
    $mensaje = '';
    $flagNombre = 0;
    $flagTipo = 0;
    foreach ($arrayHamburgesa as $p) {
        if ($p->getTipo() == $tipo) {
            $flagTipo++;
        }
        if ($p->getNombre() == $nombre) {
            $flagNombre++;
        }
        if ($flagNombre && $flagTipo) {
            $mensaje = 'Si hay';
            break;
        }
    }
    $flagTipo == 0 ? $mensaje .= 'No hay del tipo seleccionado. ' : '';
    $flagNombre == 0 ? $mensaje .= 'No hay del nombre seleccionado. ' : '';
    echo $mensaje;
} else {
    throw new Error('Parametros de la petición no validos/incompletos', 400);
}