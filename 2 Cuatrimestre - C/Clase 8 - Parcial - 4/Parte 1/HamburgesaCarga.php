<?php

include_once "./Hamburgesa.php";

if (isset($_POST["nombre"]) && isset($_POST["precio"]) && isset($_POST["tipo"]) && isset($_POST["cantidad"]) && isset($_FILES["imagen"])) {
    $nombre = $_POST["nombre"];
    $tipo = strtolower($_POST["tipo"]);
    $precio = $_POST["precio"];
    $cantidad = $_POST["cantidad"];
    try {
        $hamburgesa = new Hamburgesa(NULL, $nombre, $precio, $tipo, $cantidad);
        $destino = "./ImagenesDeHamburgesas/" . $tipo . '-' . $nombre;
        move_uploaded_file($_FILES["imagen"]["tmp_name"], $destino);
        Hamburgesa::ManejadorAgregarHamburgesa($hamburgesa);
        echo 'Se cargo la hamburgesa correctamente';
    } catch (Error $ex) {
        throw $ex;
    }
} else {
    throw new Error('Parametros de la petición no validos/incompletos.', 400);
}