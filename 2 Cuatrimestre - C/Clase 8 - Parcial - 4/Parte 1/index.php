<?php
date_default_timezone_set('America/Argentina/Buenos_Aires');
try {
    switch ($_SERVER["REQUEST_METHOD"]) {
        case 'GET':
            break;
        case 'POST':
            if ($_SERVER["REQUEST_URI"] == '/HamburgesaCarga') {
                include_once './HamburgesaCarga.php';
            } else if ($_SERVER["REQUEST_URI"] == '/Consulta') {
                include_once "./HamburgesaConsultar.php";
            } else if ($_SERVER["REQUEST_URI"] == '/Venta') {
                include_once './AltaVenta.php';
            }
            break;
        case 'PUT':
            break;
        case 'DELETE':
            break;
    }
} catch (Error $ex) {
    print('Error: ' . $ex->getMessage() . ' Status code: ' . $ex->getCode());
}