<?php

class Hamburgesa implements JsonSerializable
{
    private int $_id;
    private string $nombre;
    private float $precio;
    private string $tipo;
    private int $cantidad;

    public function __construct($id, $nombre, $precio, $tipo, $cantidad)
    {
        try {
            $this->setNombre($nombre);
            $this->setPrecio($precio);
            $this->setTipo($tipo);
            $this->setCantidad($cantidad);
            $this->setId($id);
        } catch (Error $ex) {
            throw $ex;
        }
    }

    /* GETTERS */
    public function getNombre()
    {
        return $this->nombre;
    }
    public function getPrecio()
    {
        return $this->precio;
    }
    public function getTipo()
    {
        return $this->tipo;
    }
    public function getCantidad()
    {
        return $this->cantidad;
    }
    public function getId()
    {
        return $this->_id;
    }

    /* SETTERS */
    private function setId($id = NULL)
    {
        $id ? $this->_id = $id : $this->_id = random_int(0, 10000);
    }
    public function setNombre($nombre)
    {
        !empty($nombre) ? $this->nombre = $nombre : throw new Error("Nombre invalido.", 400);
    }
    public function setPrecio($precio)
    {
        $precio > 0 ? $this->precio = $precio : throw new Error("Precio invalido.", 400);
    }
    public function setTipo($tipo)
    {
        $tipo = strtolower($tipo);
        $tipo == "simple" || $tipo == "doble" ? $this->tipo = $tipo : throw new Error("Tipo invalido.", 400);
    }
    public function setCantidad($cantidad)
    {
        $cantidad >= 0 ? $this->cantidad = $cantidad : throw new Error("Cantidad invalida.", 400);
    }

    /* JSON */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }
    public static function GuardarHamburgesaJSON($arrayHamburgesa)
    {
        if (is_array($arrayHamburgesa)) {
            $archivo = fopen("Hamburgesa.json", "w");
            if ($archivo != FALSE) {
                $json = json_encode($arrayHamburgesa);
                fputs($archivo, $json);
                fclose($archivo);
            }
        } else {
            throw new Error('El dato ingresado no es un array.', 500);
        }
    }
    public static function CargarHamburgesaArrayJSON()
    {
        $array = [];
        if (file_exists("./Hamburgesa.json")) {
            $archivo = fopen("Hamburgesa.json", "r");
            if ($archivo != FALSE) {
                $mensaje = fread($archivo, filesize("Hamburgesa.json"));
                $arrayAux = json_decode($mensaje);
                $array = Hamburgesa::GenerarArrayHamburgesas($arrayAux);
                fclose($archivo);
            }
        }
        return $array;
    }
    private static function GenerarArrayHamburgesas($array)
    {
        $arrayNew = [];
        foreach ($array as $v) {
            $hamburgesa = new Hamburgesa($v->_id, $v->nombre, $v->precio, $v->tipo, $v->cantidad);
            array_push($arrayNew, $hamburgesa);
        }
        return $arrayNew;
    }

    /* FUNCIONES */
    public function Equals($tipo, $nombre)
    {
        if ($this->tipo == $tipo && $this->nombre == $nombre) {
            return true;
        }
        return false;
    }
    private static function ExisteHamburgesa($array, $tipo, $nombre)
    {
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i]->Equals($tipo, $nombre)) {
                return $i;
            }
        }
        return -1;
    }
    public static function ManejadorAgregarHamburgesa($hamburgesa)
    {
        $arrayHamburgesas = Hamburgesa::CargarHamburgesaArrayJSON();
        $index = Hamburgesa::ExisteHamburgesa($arrayHamburgesas, $hamburgesa->tipo, $hamburgesa->nombre);
        if ($index == -1) {
            Hamburgesa::AgregarHamburgesaLista($arrayHamburgesas, $hamburgesa);
        } else {
            Hamburgesa::ManejadorStock($arrayHamburgesas, $hamburgesa, $index, true);
        }
    }
    public static function ManejadorQuitarHamburgesa($hamburgesa)
    {
        $arrayHamburgesas = Hamburgesa::CargarHamburgesaArrayJSON();
        $index = Hamburgesa::ExisteHamburgesa($arrayHamburgesas, $hamburgesa->tipo, $hamburgesa->nombre);
        if ($index == -1) {
            throw new Error("No existe el tipo o nombre de la hamburgesa seleccionada.", 400);
        } else {
            if ($arrayHamburgesas[$index]->getCantidad() < $hamburgesa->getCantidad()) {
                throw new Error("No hay suficiente stock de hamburgesa para el pedido realizado.", 502);
            }
            Hamburgesa::ManejadorStock($arrayHamburgesas, $hamburgesa, $index, false);
        }
    }
    private static function ManejadorStock($arrayHamburgesas, $hamburgesa, $index, $manejo)
    {
        switch ($manejo) {
            case true:
                $arrayHamburgesas[$index]->cantidad += $hamburgesa->cantidad;
                break;
            case false:
                $arrayHamburgesas[$index]->cantidad -= $hamburgesa->cantidad;
                break;
        }
        Hamburgesa::GuardarHamburgesaJSON($arrayHamburgesas);
    }
    private static function AgregarHamburgesaLista($arrayHamburgesas, $hamburgesa)
    {
        array_push($arrayHamburgesas, $hamburgesa);
        Hamburgesa::GuardarHamburgesaJSON($arrayHamburgesas);
    }
    public static function DevolverHamburgesa($tipo, $nombre)
    {
        $arrayHamburgesas = Hamburgesa::CargarHamburgesaArrayJSON();
        $index = Hamburgesa::ExisteHamburgesa($arrayHamburgesas, $tipo, $nombre);
        if ($index == -1) {
            throw new Error("No existe el tipo o nombre de hamburgesa seleccionado.", 400);
        } else {
            return $arrayHamburgesas[$index];
        }
    }
    public static function ListarHamburgesa($hamburgesa)
    {
        return 'Nombre: ' . $hamburgesa->getNombre() . ', precio unitario: ' . $hamburgesa->getPrecio() . ', tipo: ' . $hamburgesa->getTipo() . ", cantidad: " . $hamburgesa->getCantidad() . ".\n";
    }
    public static function ListarHamburgesas($array)
    {
        $mensaje = '';
        foreach ($array as $v) {
            $mensaje .= Hamburgesa::ListarHamburgesa($v);
        }
        return $mensaje;
    }
}