<?php
include_once './Devolucion.php';
include_once "./Ventas.php";
if (isset($_POST["numeroPedido"]) && isset($_POST["razonDevolucion"]) && isset($_FILES["imagen"])) {

} else {
    throw new Error('Parametros de la petición no validos/incompletos', 400);
}

function Devolver($arrayVentas, $arrayDevolucion, $numeroPedido, $razonDevolucion)
{
    $flag = 0;
    foreach ($arrayVentas as $k => $v) {
        if ($v->getNumeroPedido() == $numeroPedido) {
            $flag = 1;
            $devolucion = new Devolucion(NULL, $razonDevolucion, $numeroPedido);
            array_push($arrayDevolucion, $devolucion);
            Devolucion::GuardarDevolucionJSON($arrayDevolucion);
            break;
        }
    }
    if (!$flag) {
        throw new Error("No existe el numero de pedido");
    }
}