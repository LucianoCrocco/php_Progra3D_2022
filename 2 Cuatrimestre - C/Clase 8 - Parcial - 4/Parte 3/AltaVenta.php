<?php
include_once './Venta.php';
include_once './Hamburgesa.php';
if (isset($_POST["email"]) && isset($_POST["nombre"]) && isset($_POST["tipo"]) && isset($_POST["cantidad"]) && isset($_FILES["imagen"])) {
    $nombre = $_POST["nombre"];
    $tipo = strtolower($_POST["tipo"]);
    $email = $_POST["email"];
    $cantidad = $_POST["cantidad"];
    try {
        $hamburgesaAux = Hamburgesa::DevolverHamburgesa($tipo, $nombre);
        $hamburgesa = new Hamburgesa(NULL, $nombre, $hamburgesaAux->getPrecio(), $tipo, $cantidad);
        Hamburgesa::ManejadorQuitarHamburgesa($hamburgesa);
        //--------------------------------------------------------------------------------------------------------//
        $venta = new Venta($_POST["email"], $hamburgesa);
        $destino = "./ImagenesDeLaVenta/" . $tipo . '-' . $nombre . "-" . explode('@', $email)[0] . "-" . $venta->getFecha();
        move_uploaded_file($_FILES["imagen"]["tmp_name"], $destino);
        $arrayVentas = Venta::CargarVentaArrayJSON();
        array_push($arrayVentas, $venta);
        Venta::GuardarVentaJSON($arrayVentas);
        echo 'Venta de hamburgesa realizada correctamente.';
    } catch (Error $ex) {
        throw $ex;
    }
} else {
    throw new Error('Parametros de la petición no validos/incompletos', 400);
}