<?php
include_once "./Hamburgesa.php";
class Venta implements JsonSerializable
{
    private int $_id;
    private $fecha;
    private int $numeroPedido;
    private string $email;
    private Hamburgesa $hamburgesaVendida;

    public function __construct($email, $hamburgesaVendida)
    {
        try {
            $this->setId();
            $this->setFecha();
            $this->setNumeroPedido();
            $this->setEmail($email);
            $this->setHamburgesaVendida($hamburgesaVendida);
        } catch (Error $ex) {
            throw $ex;
        }
    }

    /* GETTERS */
    public function getFecha()
    {
        return $this->fecha;
    }
    public function getNumeroPedido()
    {
        return $this->numeroPedido;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getHamburgesaVendida()
    {
        return $this->hamburgesaVendida;
    }
    public function getId()
    {
        return $this->_id;
    }

    /* SETTERS */
    private function setId()
    {
        $this->_id = random_int(0, 10000);
    }
    public function setFecha()
    {
        $this->fecha = date("d-m-y H:i:s");
    }
    public function setNumeroPedido()
    {
        $this->numeroPedido = random_int(0, 10000);
    }
    public function setEmail($email)
    {
        if (!empty($email)) {
            $this->email = strtolower($email);
        } else {
            throw new Error("Email invalido.", 400);
        }
    }
    public function setHamburgesaVendida($hamburgesaVendida)
    {
        if (!empty($hamburgesaVendida)) {
            if ($hamburgesaVendida instanceof Hamburgesa) {
                $this->hamburgesaVendida = new Hamburgesa($hamburgesaVendida->getId(), $hamburgesaVendida->getNombre(), $hamburgesaVendida->getPrecio(), $hamburgesaVendida->getTipo(), $hamburgesaVendida->getCantidad());
            } else {
                $this->hamburgesaVendida = new Hamburgesa($hamburgesaVendida->_id, $hamburgesaVendida->nombre, $hamburgesaVendida->precio, $hamburgesaVendida->tipo, $hamburgesaVendida->cantidad);
            }
        } else {
            throw new Error("La hamburgesa vendida invalida.", 400);
        }
    }

    /* JSON */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }
    public static function GuardarVentaJSON($arrayVenta)
    {
        if (is_array($arrayVenta)) {
            $archivo = fopen("Venta.json", "w");
            if ($archivo != FALSE) {
                $json = json_encode($arrayVenta);
                fputs($archivo, $json);
                fclose($archivo);
            }
        } else {
            throw new Error('El dato ingresado no es un array.', 500);
        }
    }
    public static function CargarVentaArrayJSON()
    {
        $array = [];
        if (file_exists("./Venta.json")) {
            $archivo = fopen("Venta.json", "r");
            if ($archivo != FALSE) {
                $mensaje = fread($archivo, filesize("Venta.json"));
                $arrayAux = json_decode($mensaje);
                $array = Venta::GenerarArrayVentas($arrayAux);
                fclose($archivo);
            }
        }
        return $array;
    }
    private static function GenerarArrayVentas($array)
    {
        $arrayNew = [];
        foreach ($array as $v) {
            $venta = new Venta($v->email, $v->hamburgesaVendida);
            $venta->_id = $v->_id;
            $venta->numeroPedido = $v->numeroPedido;
            $venta->fecha = $v->fecha;
            array_push($arrayNew, $venta);
        }
        return $arrayNew;
    }
}