<?php
class Devolucion implements JsonSerializable
{
    public int $_id;
    public string $razonDevolucion;
    public string $numeroPedidoDevuelto;
    public function __construct($id, $razonDevolucion, $numeroPedido)
    {
        $id ? $this->_id = $id : $this->_id = random_int(0, 10000);
        $this->razonDevolucion = $razonDevolucion;
        $this->numeroPedidoDevuelto = $numeroPedido;
    }
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }
    public static function GuardarDevolucionJSON($arrayDevolucion)
    {
        if (is_array($arrayDevolucion)) {
            $archivo = fopen("Devolucion.json", "w");
            if ($archivo != FALSE) {
                $json = json_encode($arrayDevolucion);
                fputs($archivo, $json);
                fclose($archivo);
            }
        } else {
            throw new Error('El dato ingresado no es un array.', 500);
        }
    }
    public static function CargarDevolucionArrayJSON()
    {
        $array = [];
        if (file_exists("./Devolucion.json")) {
            $archivo = fopen("Devolucion.json", "r");
            if ($archivo != FALSE) {
                $mensaje = fread($archivo, filesize("Devolucion.json"));
                $arrayAux = json_decode($mensaje);
                $array = Devolucion::GenerarArrayDevolucions($arrayAux);
                fclose($archivo);
            }
        }
        return $array;
    }
    private static function GenerarArrayDevolucions($array)
    {
        $arrayNew = [];
        foreach ($array as $v) {
            $devolucion = new Devolucion($v->_id, $v->numeroPedido, $v->razonDevolucion);
            array_push($arrayNew, $devolucion);
        }
        return $arrayNew;
    }
}