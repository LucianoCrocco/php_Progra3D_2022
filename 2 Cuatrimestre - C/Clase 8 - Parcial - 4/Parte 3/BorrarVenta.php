<?php
include_once "./Venta.php";
parse_str(file_get_contents("php://input"), $put_vars);

if (isset($put_vars["numeroPedido"])) {
    $numeroPedido = $put_vars["numeroPedido"];
    try {
        $arrayVentas = Venta::CargarVentaArrayJSON();
        $mensaje = BorrarVenta($arrayVentas, $numeroPedido);
        echo $mensaje;
    } catch (Error $ex) {
        throw $ex;
    }
} else {
    throw new Error('Parametros de la petición no validos/incompletos.', 400);
}

function BorrarVenta($arrayVentas, $numeroPedido)
{
    $mensaje = "No se encontro el pedido del usuario.";
    foreach ($arrayVentas as $k => $v) {
        if ($v->getNumeroPedido() == $numeroPedido) {
            $mensaje = "Se borro la Venta.";
            unset($arrayVentas[$k]);
            $nombre = $v->getHamburgesaVendida()->getTipo() . '-' . $v->getHamburgesaVendida()->getNombre() . "-" . explode('@', $v->getHamburgesaVendida()->getEmail())[0] . "-" . $v->getFecha();
            $destino = "./BACKUPVENTAS/" . $nombre;
            rename("./ImagenesDeLaVenta/" . $nombre, $destino);
            Venta::GuardarVentaJSON($arrayVentas);
            break;
        }
    }
    return $mensaje;
}