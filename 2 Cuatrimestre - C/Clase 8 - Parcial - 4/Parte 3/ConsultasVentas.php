<?php
include_once "./Venta.php";

if (isset($_GET["nombre"]) && isset($_GET["email"])) {
    $mensaje = '';
    $arrayVentas = Venta::CargarVentaArrayJSON();
    $nombre = $_GET["nombre"];
    $email = $_GET["email"];
    $fechaInicio = NULL;
    $fechaFinal = NULL;
    $fechaHoy = date("d-m-y H:i:s");
    !isset($_GET["incio"]) ? $fechaInicio = new DateTime("yesterday") : $fechaInicio = new DateTime($inicio);
    !isset($_GET["final"]) ? $fechaFinal = $fechaHoy : $fechaFinal = date($final);

    $cantidadVendido = cantidadHamburgesasVendidas($arrayVentas);
    $mensaje .= 'Cantidad de hamburgesas vendidas: ' . $cantidadVendido . '.' . "\n\n";

    $arrayHamburgesasVendidasFechas = ventasEntreFechas($arrayVentas, $inicio, $final);
    $mensaje .= 'Listado de hamburgesas entre dos fechas: ' . "\n";
    $mensaje .= Hamburgesa::ListarHamburgesas($arrayHamburgesasVendidasFechas) . "\n";

    //ROMPE
    $arrayVentasUsuario = ventasUsuario($arrayVentas, $email);
    $mensaje .= 'Cantidad de hamburgesas vendidas al usuario ingresado: ' . "\n";
    $mensaje .= Hamburgesa::ListarHamburgesas($arrayVentasUsuario) . "\n";

    $arrayVentasNombre = ventasNombre($arrayVentas, $nombre);
    $mensaje .= 'Cantidad de hamburgesas vendidas del nombre ingresado: ' . "\n";
    $mensaje .= Hamburgesa::ListarHamburgesas($arrayVentasNombre) . "\n";

    $mensaje = trim($mensaje, "\n");
    echo $mensaje;
} else {
    throw new Error('Parametros de la petición no validos/incompletos', 400);
}

//-------------------Funciones-------------------
function cantidadHamburgesasVendidas($arrayVentas)
{
    $contador = 0;
    foreach ($arrayVentas as $v) {
        $contador += $v->getHamburgesaVendida()->getCantidad();
    }
    return $contador;
}

function ventasEntreFechas($arrayVentas, $inicio, $final)
{
    $arrayVentasFechas = [];
    $fechaInicio = NULL;
    $fechaFinal = NULL;
    $fechaHoy = date("d-m-y");
    !$inicio ? $fechaInicio = date("d-m-y", strtotime("-1 day")) : $fechaInicio = date($inicio);
    !$final ? $fechaFinal = $fechaHoy : $fechaFinal = date($final);

    foreach ($arrayVentas as $v) {
        if ($v->getFecha() >= $fechaInicio && $v->getFecha() <= $fechaFinal) {
            array_push($arrayVentasFechas, $v->getHamburgesaVendida());
        }
    }

    return $arrayVentasFechas;
}

function ventasUsuario($arrayVentas, $email)
{
    $arrayUsuarioVentas = [];
    foreach ($arrayVentas as $v) {
        if ($v->getEmail() == $email) {
            array_push($arrayUsuarioVentas, $v->getHamburgesaVendida());
        }
    }
    return $arrayUsuarioVentas;
}

function ventasNombre($arrayVentas, $nombre)
{
    $arrayNombreVentas = [];
    foreach ($arrayVentas as $v) {
        $hamburgesa = $v->getHamburgesaVendida();
        if ($hamburgesa->getNombre() == $nombre) {
            array_push($arrayNombreVentas, $hamburgesa);
        }
    }
    return $arrayNombreVentas;
}