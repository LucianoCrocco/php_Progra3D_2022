<?php
include_once './Modelos/Venta.php';
parse_str(file_get_contents("php://input"), $put_vars);

if (isset($put_vars["numeroPedido"]) && isset($put_vars["email"]) && isset($put_vars["nombre"]) && isset($put_vars["tipo"]) && isset($put_vars["cantidad"])) {
    $numeroPedido = $put_vars["numeroPedido"];
    $email = $put_vars["email"];
    $nombre = $put_vars["nombre"];
    $tipo = $put_vars["tipo"];
    $cantidad = $put_vars["cantidad"];
    try {
        $flagModifico = false;
        $arrayVentas = Venta::CargarVentaArrayJSON();
        foreach ($arrayVentas as $v) {
            if ($v->numeroPedido == $numeroPedido && $v->email == $email) {
                $origen = $v->hamburgesaVendida->tipo . '-' . $v->hamburgesaVendida->nombre . "-" . explode('@', $v->email)[0] . "-" . $v->fecha;
                $v->hamburgesaVendida->nombre = $nombre;
                $v->hamburgesaVendida->tipo = $tipo;
                $v->hamburgesaVendida->cantidad = $cantidad;
                $flagModifico = true;
                $destino = $v->hamburgesaVendida->tipo . '-' . $v->hamburgesaVendida->nombre . "-" . explode('@', $v->email)[0] . "-" . $v->fecha;
                rename("./ImagenesDeLaVenta/" . $origen, "./ImagenesDeLaVenta/" . $destino);
                break;
            }
        }

        Venta::GuardarVentaJSON($arrayVentas);
        $flagModifico ? $mensaje = "Venta modificada correctamente" : $mensaje = "No se encontro la venta, no se modifico";

        echo $mensaje;
    } catch (Error $ex) {
        throw $ex;
    }
} else {
    throw new Error('Parametros de la petición no validos/incompletos.', 400);
}