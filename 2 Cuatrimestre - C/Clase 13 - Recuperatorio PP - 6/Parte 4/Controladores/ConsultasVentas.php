<?php
include_once "./Modelos/Venta.php";

if (isset($_GET["tipo"]) && isset($_GET["email"])) {
    $mensaje = '';
    $arrayVentas = Venta::CargarVentaArrayJSON();
    $tipo = $_GET["tipo"];
    $email = $_GET["email"];
    $fechaHoy = date("d-m-y");
    !isset($_GET["incio"]) ? $fechaInicio = date("d-m-y", strtotime("-1 day")) : $fechaInicio = date($_GET["inicio"]);
    !isset($_GET["final"]) ? $fechaFinal = $fechaHoy : $fechaFinal = date($_GET["final"]);


    $cantidadVendido = cantidadHamburgesasVendidas($arrayVentas);
    $mensaje .= 'Cantidad de hamburgesas vendidas: ' . $cantidadVendido . '.' . "\n\n";

    $arrayHamburgesasVendidasFechas = ventasEntreFechas($arrayVentas, $fechaInicio, $fechaFinal);
    $mensaje .= 'Listado de hamburgesas entre el ' . $fechaInicio . ' y ' . $fechaFinal . ': ' . "\n";
    $mensaje .= Hamburgesa::ListarHamburgesas($arrayHamburgesasVendidasFechas) . "\n";

    $arrayVentasUsuario = ventasUsuario($arrayVentas, $email);
    $mensaje .= 'Cantidad de hamburgesas vendidas al usuario ingresado: ' . "\n";
    $mensaje .= Hamburgesa::ListarHamburgesas($arrayVentasUsuario) . "\n";

    $arrayVentasNombre = ventasNombre($arrayVentas, $tipo);
    $mensaje .= 'Cantidad de hamburgesas vendidas del tipo ingresado: ' . "\n";
    $mensaje .= Hamburgesa::ListarHamburgesas($arrayVentasNombre) . "\n";

    $mensaje = trim($mensaje, "\n");
    echo $mensaje;
} else {
    throw new Error('Parametros de la petición no validos/incompletos', 400);
}

//-------------------Funciones-------------------
function cantidadHamburgesasVendidas($arrayVentas)
{
    $contador = 0;
    foreach ($arrayVentas as $v) {
        $contador += $v->hamburgesaVendida->cantidad;
    }
    return $contador;
}

function ventasEntreFechas($arrayVentas, $inicio, $final)
{
    $arrayVentasFechas = [];

    foreach ($arrayVentas as $v) {
        $fechaVenta = explode(" ", $v->fecha)[0];
        if ($fechaVenta >= $inicio && $fechaVenta <= $final) {
            array_push($arrayVentasFechas, $v->hamburgesaVendida);
        }
        usort($arrayVentasFechas, "compararPorNombre");
    }

    return $arrayVentasFechas;
}

function ventasUsuario($arrayVentas, $email)
{
    $arrayUsuarioVentas = [];
    foreach ($arrayVentas as $v) {
        if ($v->email == $email) {
            array_push($arrayUsuarioVentas, $v->hamburgesaVendida);
        }
    }
    return $arrayUsuarioVentas;
}

function ventasNombre($arrayVentas, $tipo)
{
    $arrayNombreVentas = [];
    foreach ($arrayVentas as $v) {
        $hamburgesa = $v->hamburgesaVendida;
        if ($hamburgesa->tipo == $tipo) {
            array_push($arrayNombreVentas, $hamburgesa);
        }
    }
    return $arrayNombreVentas;
}

function compararPorNombre($a, $b)
{
    if ($a->nombre == $b->nombre) {
        return 0;
    }
    return ($a->nombre < $b->nombre) ? -1 : 1;
}