<?php
include_once './Modelos/Devolucion.php';
include_once './Modelos/Cupon.php';
include_once "./Modelos/Venta.php";
if (isset($_POST["numeroPedido"]) && isset($_POST["email"]) && isset($_POST["razonDevolucion"]) && isset($_FILES["imagen"])) {
    $numeroPedido = $_POST["numeroPedido"];
    $email = $_POST["email"];
    $razonDevolucion = strtolower($_POST["razonDevolucion"]);
    try {
        Devolver($numeroPedido);

        $cupon = new Cupon(NULL, $email, false, NULL);
        $devolucion = new Devolucion(NULL, $razonDevolucion, $numeroPedido, $cupon->id);

        $arrayDevolucion = Devolucion::CargarDevolucionArrayJSON();
        array_push($arrayDevolucion, $devolucion);
        Devolucion::GuardarDevolucionJSON($arrayDevolucion);

        $arrayCupones = Cupon::CargarCuponArrayJSON();
        array_push($arrayCupones, $cupon);
        Cupon::GuardarCuponesJSON($arrayCupones);


        $destino = "./ImagenesClientesEnojados/" . $numeroPedido . '-' . $razonDevolucion . "-" . $cupon->fecha;
        move_uploaded_file($_FILES["imagen"]["tmp_name"], $destino);

        echo json_encode(("Id codigo de descuento:" . $cupon->id));
    } catch (Error $ex) {
        throw $ex;
    }
} else {
    throw new Error('Parametros de la petición no validos/incompletos', 400);
}

function Devolver($numeroPedido)
{
    $arrayVentas = Venta::CargarVentaArrayJSON();
    $flag = 0;
    foreach ($arrayVentas as $k => $v) {
        if ($v->numeroPedido == $numeroPedido) {
            unset($arrayVentas[$k]);
            $flag = 1;
            break;
        }
    }

    if (!$flag) {
        throw new Error("No existe el numero de pedido");
    }
    Venta::GuardarVentaJSON($arrayVentas);
}