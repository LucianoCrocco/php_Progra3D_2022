<?php
include_once './Modelos/Venta.php';
include_once './Modelos/Hamburgesa.php';
include_once './Modelos/Cupon.php';
if (isset($_POST["email"]) && isset($_POST["nombre"]) && isset($_POST["tipo"]) && isset($_POST["cantidad"]) && isset($_FILES["imagen"])) {
    $nombre = $_POST["nombre"];
    $tipo = strtolower($_POST["tipo"]);
    $email = $_POST["email"];
    $cantidad = $_POST["cantidad"];
    $idDescuento = false;
    $descuento = false;
    try {

        $hamburgesaAux = Hamburgesa::DevolverHamburgesa($tipo, $nombre);
        $hamburgesa = new Hamburgesa(NULL, $nombre, $hamburgesaAux->precio, $tipo, $cantidad);

        if (isset($_POST["idCuponDescuento"])) {
            $idDescuento = $_POST["idCuponDescuento"];
            $existe = Cupon::VerificarExistenciaCupon($idDescuento);
            if ($existe) {
                $hamburgesa->precio = $hamburgesa->precio * 0.9;
                Cupon::MarcarCupon($idDescuento);
                $descuento = true;
            }
        }

        Hamburgesa::ManejadorQuitarHamburgesa($hamburgesa);
        //--------------------------------------------------------------------------------------------------------//
        $venta = new Venta($_POST["email"], $hamburgesa, $descuento);
        $destino = "./ImagenesDeLaVenta/" . $tipo . '-' . $nombre . "-" . explode('@', $email)[0] . "-" . $venta->fecha;
        move_uploaded_file($_FILES["imagen"]["tmp_name"], $destino);
        $arrayVentas = Venta::CargarVentaArrayJSON();
        array_push($arrayVentas, $venta);
        Venta::GuardarVentaJSON($arrayVentas);
        echo 'Venta de hamburgesa realizada correctamente.';
    } catch (Error $ex) {
        throw $ex;
    }
} else {
    throw new Error('Parametros de la petición no validos/incompletos', 400);
}