<?php
include_once "./Modelos/Venta.php";
try {
    $arrayVentas = Venta::CargarVentaArrayJSON();
    Venta::GenerarCSV($arrayVentas);
    header('Content-Type: text/csv');
    header("Content-disposition: attachment; filename=dataProductos.csv");
    readfile("./CSV/dataVentas.csv");
} catch (Error $ex) {
    throw $ex;
}