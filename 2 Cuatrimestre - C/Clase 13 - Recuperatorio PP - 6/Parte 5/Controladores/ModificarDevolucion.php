<?php
include_once './Modelos/Devolucion.php';
parse_str(file_get_contents("php://input"), $put_vars);

if (isset($put_vars["numeroPedido"]) && isset($put_vars["cuponDado"]) && isset($put_vars["nuevaRazon"])) {
    $numeroPedido = $put_vars["numeroPedido"];
    $cuponDado = $put_vars["cuponDado"];
    $razonDevolucion = strtolower($put_vars["nuevaRazon"]);
    try {
        $flagModifico = false;
        $arrayDevolucion = Devolucion::CargarDevolucionArrayJSON();
        foreach ($arrayDevolucion as &$v) {
            if ($v->idCuponDado == $cuponDado && $v->numeroPedidoDevuelto == $numeroPedido) {
                $v->razonDevolucion = $razonDevolucion;
                $flagModifico = true;
                break;
            }
        }

        Devolucion::GuardarDevolucionJSON($arrayDevolucion);
        $flagModifico ? $mensaje = "Devolucion modificada correctamente" : $mensaje = "No se encontro la devolucion, no se modifico";

        echo $mensaje;
    } catch (Error $ex) {
        throw $ex;
    }
} else {
    throw new Error('Parametros de la petición no validos/incompletos.', 400);
}