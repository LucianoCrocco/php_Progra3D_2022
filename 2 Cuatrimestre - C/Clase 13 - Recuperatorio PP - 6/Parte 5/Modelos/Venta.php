<?php
include_once "./Modelos/Hamburgesa.php";
include_once "./Modelos/Devolucion.php";
class Venta implements JsonSerializable
{
    public int $id;
    public $fecha;
    public int $numeroPedido;
    public string $email;
    public Hamburgesa $hamburgesaVendida;
    public $descuento;

    public function __construct($email, $hamburgesaVendida, $descuento = false)
    {
        try {
            $this->setId();
            $this->setFecha();
            $this->setNumeroPedido();
            $this->setEmail($email);
            $this->setHamburgesaVendida($hamburgesaVendida);
            $this->descuento = $descuento;
        } catch (Error $ex) {
            throw $ex;
        }
    }

    /* SETTERS */
    private function setId()
    {
        $this->id = random_int(0, 10000);
    }
    public function setFecha()
    {
        $this->fecha = date("d-m-y H:i:s");
    }
    public function setNumeroPedido()
    {
        $this->numeroPedido = random_int(0, 10000);
    }
    public function setEmail($email)
    {
        if (!empty($email)) {
            $this->email = strtolower($email);
        } else {
            throw new Error("Email invalido.", 400);
        }
    }
    public function setHamburgesaVendida($hamburgesaVendida)
    {
        if (!empty($hamburgesaVendida)) {
            if ($hamburgesaVendida instanceof Hamburgesa) {
                $this->hamburgesaVendida = new Hamburgesa($hamburgesaVendida->id, $hamburgesaVendida->nombre, $hamburgesaVendida->precio, $hamburgesaVendida->tipo, $hamburgesaVendida->cantidad);
            } else {
                $this->hamburgesaVendida = new Hamburgesa($hamburgesaVendida->id, $hamburgesaVendida->nombre, $hamburgesaVendida->precio, $hamburgesaVendida->tipo, $hamburgesaVendida->cantidad);
            }
        } else {
            throw new Error("La hamburgesa vendida invalida.", 400);
        }
    }

    /* JSON */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }
    public static function GuardarVentaJSON($arrayVenta)
    {
        if (is_array($arrayVenta)) {
            $archivo = fopen("./Data/Venta.json", "w");
            if ($archivo != FALSE) {
                $json = json_encode($arrayVenta);
                fputs($archivo, $json);
                fclose($archivo);
            }
        } else {
            throw new Error('El dato ingresado no es un array.', 500);
        }
    }
    public static function CargarVentaArrayJSON()
    {
        $array = [];
        if (file_exists("./Data/Venta.json")) {
            $archivo = fopen("./Data/Venta.json", "r");
            if ($archivo != FALSE) {
                $mensaje = fread($archivo, filesize("./Data/Venta.json"));
                $arrayAux = json_decode($mensaje);
                $array = Venta::GenerarArrayVentas($arrayAux);
                fclose($archivo);
            }
        }
        return $array;
    }
    private static function GenerarArrayVentas($array)
    {
        $arrayNew = [];
        foreach ($array as $v) {
            $venta = new Venta($v->email, $v->hamburgesaVendida, $v->descuento);
            $venta->id = $v->id;
            $venta->numeroPedido = $v->numeroPedido;
            $venta->fecha = $v->fecha;
            array_push($arrayNew, $venta);
        }
        return $arrayNew;
    }

    public static function GenerarCSV($ventas)
    {
        if (is_array($ventas)) {
            $archivo = fopen("./CSV/dataVentas.csv", "w");
            if ($archivo != FALSE) {
                fputs($archivo, 'id,fecha,numeroPedido,email,descuento,porcentaje,cupon' . PHP_EOL);
                foreach ($ventas as $v) {
                    $mensajeCupon = "";
                    $cupon = Devolucion::TraerCuponPorPedido($v->numeroPedido);
                    $cupon ? $mensajeCupon = ($cupon->usado ? "utilizado" : "no utilizado") : $mensajeCupon = "no tiene cupon";
                    $string = $v->id . "," . $v->fecha . "," . $v->numeroPedido . "," . ($v->descuento ? "si,10" : "no,0") . "," . $mensajeCupon;
                    fputs($archivo, $string . PHP_EOL);
                }
                fclose($archivo);
            }
        }
    }
}