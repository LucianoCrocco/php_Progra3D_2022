<?php
include_once "./Modelos/Cupon.php";
class Devolucion implements JsonSerializable
{
    public int $id;
    public string $razonDevolucion;
    public string $numeroPedidoDevuelto;
    public int $idCuponDado;
    public function __construct($id, $razonDevolucion, $numeroPedido, $idCuponDado)
    {
        $id ? $this->id = $id : $this->id = random_int(0, 10000);
        $this->razonDevolucion = $razonDevolucion;
        $this->numeroPedidoDevuelto = $numeroPedido;
        $this->idCuponDado = $idCuponDado;
    }
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }
    public static function GuardarDevolucionJSON($arrayDevolucion)
    {
        if (is_array($arrayDevolucion)) {
            $archivo = fopen("./Data/Devoluciones.json", "w");
            if ($archivo != FALSE) {
                $json = json_encode($arrayDevolucion);
                fputs($archivo, $json);
                fclose($archivo);
            }
        } else {
            throw new Error('El dato ingresado no es un array.', 500);
        }
    }
    public static function CargarDevolucionArrayJSON()
    {
        $array = [];
        if (file_exists("./Data/Devoluciones.json")) {
            $archivo = fopen("./Data/Devoluciones.json", "r");
            if ($archivo != FALSE) {
                $mensaje = fread($archivo, filesize("./Data/Devoluciones.json"));
                $arrayAux = json_decode($mensaje);
                $array = Devolucion::GenerarArrayDevoluciones($arrayAux);
                fclose($archivo);
            }
        }
        return $array;
    }
    private static function GenerarArrayDevoluciones($array)
    {
        $arrayNew = [];
        foreach ($array as $v) {
            $devolucion = new Devolucion($v->id, $v->razonDevolucion, $v->numeroPedidoDevuelto, $v->idCuponDado);
            array_push($arrayNew, $devolucion);
        }
        return $arrayNew;
    }
    public static function ListarDevoluciones($array)
    {
        $mensaje = '';
        foreach ($array as $v) {
            $mensaje .= Devolucion::ListarDevolucion($v);
        }
        return $mensaje;
    }
    private static function ListarDevolucion($devolucion)
    {
        return 'Razon devolucion: ' . $devolucion->razonDevolucion . ', Pedido devuelto: ' . $devolucion->numeroPedidoDevuelto . ', Id cupon dado:' . $devolucion->idCuponDado . ".\n";
    }

    public static function TraerCuponPorPedido($numeroPedido)
    {
        $arrayDevoluciones = Devolucion::CargarDevolucionArrayJSON();
        $arrayCupones = Cupon::CargarCuponArrayJSON();

        foreach ($arrayDevoluciones as $v) {
            if ($v->numeroPedidoDevuelto == $numeroPedido) {
                $cupon = Cupon::TraerCupon($arrayCupones, $v->idCuponDado);
                return $cupon;
            }
        }
        return false;

    }
}