<?php
// Error Handling
error_reporting(-1);
ini_set('display_errors', 1);

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Factory\AppFactory;
use Slim\Routing\RouteCollectorProxy;
use Slim\Routing\RouteContext;

require __DIR__ . '/../vendor/autoload.php';

require_once './db/AccesoDatos.php';
require_once './middlewares/Logger.php';
require_once './controllers/UsuarioController.php';

// Load ENV
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->safeLoad();

// Instantiate App
$app = AppFactory::create();

// Set base path
//$app->setBasePath('/app'); -> No hace falta, lo levanto con PHP

// Add error middleware
$app->addErrorMiddleware(true, true, true);

// Add parse body
$app->addBodyParsingMiddleware();

// Routes
$app->group('/usuarios', function (RouteCollectorProxy $group) {
    $group->get(\UsuarioController::class . ':TraerTodos');// [/] es opcional ya que de por sí entra a la raíz de /usuarios. Puedo poner un string vacio u omitirlo.
    $group->get('/{usuario}', \UsuarioController::class . ':TraerUno'); // /{usuario} es la variable que recibe despues de la / de ruteo. Es el nombre de la variable donde se guarda despues de la barra de la url. args toma este {usuario}. https://slim-php-heroku-facultad.herokuapp.com/usuarios/franco -> De esta manera "franco" despues de la / se almacena en usuario.
    $group->post(\UsuarioController::class . ':CargarUno');
  })/*->add(\Logger::class . ':LogOperacion')->add(\Logger::class . ':LogOperacion')*/; //De esta manera agrego mas de un Middleware.

$app->get('[/]', function (Request $request, Response $response) {    
    $response->getBody()->write("Slim Framework 4 PHP");
    return $response;
});


//Ejercicio 1
$app->group('/credenciales', function (RouteCollectorProxy $group){
  $group->get('[/]', \UsuarioController::class . ':TraerUno');
  $group->post('[/]', \UsuarioController::class . ':TraerUno');
})->add(\Logger::class . ':VerificadorDeCredenciales');

//Ejercicio 2

$app->group('/json', function (RouteCollectorProxy $group){
  $group->get("[/]", \UsuarioController::class . ':TraerUno');
})->add(\Logger::class . ':VerificadorDeCredencialesJSON');

$app->run();
