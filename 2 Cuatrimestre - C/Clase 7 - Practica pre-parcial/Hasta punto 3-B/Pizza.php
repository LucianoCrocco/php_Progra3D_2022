<?php

class Pizza implements JsonSerializable
{
    private int $_id;
    private string $sabor;
    private float $precio;
    private string $tipo;
    private int $cantidad;

    public function __construct($sabor, $precio, $tipo, $cantidad)
    {
        try {
            $this->setId();
            $this->setSabor($sabor);
            $this->setPrecio($precio);
            $this->setTipo($tipo);
            $this->setCantidad($cantidad);
        } catch (Error $ex) {
            throw $ex;
        }
    }

    /* GETTERS */
    public function getSabor()
    {
        return $this->sabor;
    }
    public function getPrecio()
    {
        return $this->precio;
    }
    public function getTipo()
    {
        return $this->tipo;
    }
    public function getCantidad()
    {
        return $this->cantidad;
    }
    public function getId()
    {
        return $this->_id;
    }

    /* SETTERS */
    private function setId()
    {
        $this->_id = random_int(0, 10000);
    }
    public function setSabor($sabor)
    {
        if (!empty($sabor)) {
            $this->sabor = $sabor;
        } else {
            throw new Error("Sabor invalido.", 400);
        }
    }
    public function setPrecio($precio)
    {
        if ($precio > 0) {
            $this->precio = $precio;
        } else {
            throw new Error("Precio invalido.", 400);
        }
    }
    public function setTipo($tipo)
    {
        $tipo = strtolower($tipo);
        if ($tipo == "molde" || $tipo == "piedra") {
            $this->tipo = $tipo;
        } else {
            throw new Error("Tipo invalido.", 400);
        }
    }
    public function setCantidad($cantidad)
    {
        if ($cantidad >= 0) {
            $this->cantidad = $cantidad;
        } else {
            throw new Error("Cantidad invalida.", 400);
        }
    }

    /* JSON */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }
    public static function GuardarPizzaJSON($arrayPizza)
    {
        if (is_array($arrayPizza)) {
            $archivo = fopen("Pizza.json", "w");
            if ($archivo != FALSE) {
                $json = json_encode($arrayPizza);
                fputs($archivo, $json);
                fclose($archivo);
            }
        } else {
            throw new Error('El dato ingresado no es un array.', 500);
        }
    }
    public static function CargarPizzaArrayJSON()
    {
        $array = [];
        if (file_exists("./Pizza.json")) {
            $archivo = fopen("Pizza.json", "r");
            if ($archivo != FALSE) {
                $mensaje = fread($archivo, filesize("Pizza.json"));
                $arrayAux = json_decode($mensaje);
                $array = Pizza::GenerarArrayPizzas($arrayAux);
                fclose($archivo);
            }
        }
        return $array;
    }
    private static function GenerarArrayPizzas($array)
    {
        $arrayNew = [];
        foreach ($array as $v) {
            $pizza = new Pizza($v->sabor, $v->precio, $v->tipo, $v->cantidad);
            $pizza->_id = $v->_id;
            array_push($arrayNew, $pizza);
        }
        return $arrayNew;
    }

    /* FUNCIONES */
    public function Equals($tipo, $sabor)
    {
        if ($this->tipo == $tipo && $this->sabor == $sabor) {
            return true;
        }
        return false;
    }
    private static function ExistePizza($array, $tipo, $sabor)
    {
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i]->Equals($tipo, $sabor)) {
                return $i;
            }
        }
        return -1;
    }
    public static function ManejadorAgregarPizza($pizza)
    {
        $arrayPizzas = Pizza::CargarPizzaArrayJSON();
        $index = Pizza::ExistePizza($arrayPizzas, $pizza->tipo, $pizza->sabor);
        if ($index == -1) {
            Pizza::AgregarPizzaLista($arrayPizzas, $pizza);
        } else {
            Pizza::ManejadorStock($arrayPizzas, $pizza, $index, true);
        }
    }
    public static function ManejadorQuitarPizza($pizza)
    {
        $arrayPizzas = Pizza::CargarPizzaArrayJSON();
        $index = Pizza::ExistePizza($arrayPizzas, $pizza->tipo, $pizza->sabor);
        if ($index == -1) {
            throw new Error("No existe el tipo o sabor de pizza seleccionado.", 400);
        } else {
            if ($arrayPizzas[$index]->getCantidad() < $pizza->getCantidad()) {
                throw new Error("No hay suficiente stock de pizza para el pedido realizado.", 502);
            }
            Pizza::ManejadorStock($arrayPizzas, $pizza, $index, false);
        }
    }
    private static function ManejadorStock($arrayPizzas, $pizza, $index, $manejo)
    {
        switch ($manejo) {
            case true:
                $arrayPizzas[$index]->cantidad += $pizza->cantidad;
                break;
            case false:
                $arrayPizzas[$index]->cantidad -= $pizza->cantidad;
                break;
        }
        Pizza::GuardarPizzaJSON($arrayPizzas);
    }
    private static function AgregarPizzaLista($arrayPizzas, $pizza)
    {
        array_push($arrayPizzas, $pizza);
        Pizza::GuardarPizzaJSON($arrayPizzas);
    }
    public static function DevolverPizza($tipo, $sabor)
    {
        $arrayPizzas = Pizza::CargarPizzaArrayJSON();
        $index = Pizza::ExistePizza($arrayPizzas, $tipo, $sabor);
        if ($index == -1) {
            throw new Error("No existe el tipo o sabor de pizza seleccionado.", 400);
        } else {
            return $arrayPizzas[$index];
        }
    }
}
