<?php

use FFI\Exception;

try {
    switch ($_SERVER["REQUEST_METHOD"]) {
        case 'GET':
            include_once './PizzaCarga.php';
            break;
        case 'POST':
            if ($_SERVER["REQUEST_URI"] == '/Consulta') {
                include_once './PizzaConsultar.php';
            } else  
            if ($_SERVER["REQUEST_URI"] == '/Venta') {
                include_once './AltaVenta.php';
            }
            break;
        case 'PUT':
            break;
        case 'DELETE':
            break;
    }
} catch (Error $ex) {
    print('Error: ' . $ex->getMessage() . ' Status code: ' . $ex->getCode());
}
