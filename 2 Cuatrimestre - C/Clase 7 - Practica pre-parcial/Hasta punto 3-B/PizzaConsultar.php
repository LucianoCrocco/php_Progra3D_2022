<?php
include_once "./Pizza.php";

if (isset($_POST["sabor"]) && isset($_POST["tipo"])) {
    $sabor = $_POST["sabor"];
    $tipo = strtolower($_POST["tipo"]);
    if ($tipo != "molde" && $tipo != "piedra") {
        throw new Error("Tipo invalido.", 400);
    }
    $arrayPizza = Pizza::CargarPizzaArrayJSON();
    $mensaje = '';
    $flagSabor = 0;
    $flagTipo = 0;
    foreach ($arrayPizza as $p) {
        if ($p->getTipo() == $tipo) {
            $flagTipo++;
        }
        if ($p->getSabor() == $sabor) {
            $flagSabor++;
        }
        if ($flagSabor && $flagTipo) {
            $mensaje = 'Si hay';
            break;
        }
    }
    $flagTipo == 0 ? $mensaje .= 'No hay del tipo seleccionado. ' : '';
    $flagSabor == 0 ? $mensaje .= 'No hay del sabor seleccionado. ' : '';
    echo $mensaje;
} else {
    throw new Error('Parametros de la petición no validos/incompletos', 400);
}
