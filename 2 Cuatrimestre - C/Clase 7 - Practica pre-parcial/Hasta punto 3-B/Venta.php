<?php
class Venta implements JsonSerializable
{
    private int $_id;
    private string $fecha;
    private int $numeroPedido;
    private string $email;
    private $pizzaVendida;

    public function __construct($email, $pizzaVendida)
    {
        try {
            $this->setId();
            $this->setFecha();
            $this->setNumeroPedido();
            $this->setEmail($email);
            $this->setPizzaVendida($pizzaVendida);
        } catch (Error $ex) {
            throw $ex;
        }
    }

    /* GETTERS */
    public function getFecha()
    {
        return $this->fecha;
    }
    public function getNumeroPedido()
    {
        return $this->numeroPedido;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getPizzaVendida()
    {
        return $this->pizzaVendida;
    }
    public function getId()
    {
        return $this->_id;
    }

    /* SETTERS */
    private function setId()
    {
        $this->_id = random_int(0, 10000);
    }
    public function setFecha()
    {
        $this->fecha = date("d-m-y");
    }
    public function setNumeroPedido()
    {
        $this->numeroPedido = random_int(0, 10000);
    }
    public function setEmail($email)
    {
        if (!empty($email)) {
            $this->email = strtolower($email);
        } else {
            throw new Error("Email invalido.", 400);
        }
    }
    public function setPizzaVendida($pizzaVendida)
    {
        if (!empty($pizzaVendida)) {
            $this->pizzaVendida = $pizzaVendida;
        } else {
            throw new Error("La pizza vendida invalida.", 400);
        }
    }

    /* JSON */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }
    public static function GuardarVentaJSON($arrayVenta)
    {
        if (is_array($arrayVenta)) {
            $archivo = fopen("Venta.json", "w");
            if ($archivo != FALSE) {
                $json = json_encode($arrayVenta);
                fputs($archivo, $json);
                fclose($archivo);
            }
        } else {
            throw new Error('El dato ingresado no es un array.', 500);
        }
    }
    public static function CargarVentaArrayJSON()
    {
        $array = [];
        if (file_exists("./Venta.json")) {
            $archivo = fopen("Venta.json", "r");
            if ($archivo != FALSE) {
                $mensaje = fread($archivo, filesize("Venta.json"));
                $arrayAux = json_decode($mensaje);
                $array = Venta::GenerarArrayVentas($arrayAux);
                fclose($archivo);
            }
        }
        return $array;
    }
    private static function GenerarArrayVentas($array)
    {
        $arrayNew = [];
        foreach ($array as $v) {
            $venta = new Venta($v->email, $v->pizzaVendida);
            $venta->_id = $v->_id;
            $venta->numeroPedido = $v->numeroPedido;
            $venta->fecha = $v->fecha;
            array_push($arrayNew, $venta);
        }
        return $arrayNew;
    }
}
