<?php
include_once './Venta.php';
include_once './Pizza.php';
if (isset($_POST["email"]) && isset($_POST["sabor"]) && isset($_POST["tipo"]) && isset($_POST["cantidad"]) && isset($_FILES["imagen"])) {
    $sabor =  $_POST["sabor"];
    $tipo = strtolower($_POST["tipo"]);
    $email = $_POST["email"];
    $cantidad = $_POST["cantidad"];
    try {
        $pizzaAux = Pizza::DevolverPizza($tipo, $sabor);
        $pizza = new Pizza($sabor, $pizzaAux->getPrecio(), $tipo, $cantidad);
        Pizza::ManejadorQuitarPizza($pizza);
        //--------------------------------------------------------------------------------------------------------//
        $venta = new Venta($_POST["email"], $pizza);
        $destino = "./ImagenesDeLaVenta/" . $tipo . '-' . $sabor . "-" . explode('@', $email)[0] . "-" . $venta->getFecha() . '-' . $_FILES["imagen"]["name"];
        move_uploaded_file($_FILES["imagen"]["tmp_name"], $destino);
        $arrayVentas = Venta::CargarVentaArrayJSON();
        array_push($arrayVentas, $venta);
        Venta::GuardarVentaJSON($arrayVentas);
        echo 'Venta de pizza realizada correctamente.';
    } catch (Error $ex) {
        throw $ex;
    }
} else {
    throw new Error('Parametros de la petición no validos/incompletos', 400);
}
