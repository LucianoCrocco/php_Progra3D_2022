<?php

include_once "./Pizza.php";

if (isset($_GET["sabor"]) && isset($_GET["precio"]) && isset($_GET["tipo"]) && isset($_GET["cantidad"])) {
    try {
        $pizza = new Pizza($_GET["sabor"], $_GET["precio"], $_GET["tipo"], $_GET["cantidad"]);
        Pizza::ManejadorAgregarPizza($pizza);
        echo 'Se cargo la pizza correctamente';
    } catch (Error $ex) {
        throw $ex;
    }
} else {
    throw new Error('Parametros de la petición no validos/incompletos.', 400);
}
