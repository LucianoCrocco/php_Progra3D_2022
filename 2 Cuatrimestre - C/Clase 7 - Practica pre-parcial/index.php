<?php
date_default_timezone_set('America/Argentina/Buenos_Aires');
try {
    switch ($_SERVER["REQUEST_METHOD"]) {
        case 'GET':
            include_once './ConsultasVentas.php';
            break;
        case 'POST':
            if ($_SERVER["REQUEST_URI"] == '/Consulta') {
                include_once './PizzaConsultar.php';
            } else if ($_SERVER["REQUEST_URI"] == '/Venta') {
                include_once './AltaVenta.php';
            } else if ($_SERVER["REQUEST_URI"] == '/PizzaCarga') {
                include_once './PizzaCarga.php';
            }
            break;
        case 'PUT':
            include_once './ModificarVenta.php';
            break;
        case 'DELETE':
            break;
    }
} catch (Error $ex) {
    print('Error: ' . $ex->getMessage() . ' Status code: ' . $ex->getCode());
}