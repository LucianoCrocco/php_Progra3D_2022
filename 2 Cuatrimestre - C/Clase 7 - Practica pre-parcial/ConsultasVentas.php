<?php
include_once "./Venta.php";

if (isset($_GET["sabor"]) && isset($_GET["email"])) {
    $mensaje = '';
    $arrayVentas = Venta::CargarVentaArrayJSON();
    $sabor = $_GET["sabor"];
    $email = $_GET["email"];
    $fechaInicio = NULL;
    $fechaFinal = NULL;
    $fechaHoy = date("d-m-y");
    !isset($_GET["incio"]) ? $fechaInicio = date("d-m-y", strtotime("-1 day")) : $fechaInicio = date($inicio);
    !isset($_GET["final"])  ? $fechaFinal = $fechaHoy : $fechaFinal = date($final);

    $cantidadVendido = cantidadPizzasVendidas($arrayVentas);
    $mensaje .= 'Cantidad de pizzas vendidas: ' . $cantidadVendido . '.' . "\n\n";

    $arrayPizzasVendidasFechas = ventasEntreFechas($arrayVentas, $inicio, $final);
    $mensaje .= 'Listado de pizzas entre dos fechas: ' . "\n";
    $mensaje .= Pizza::ListarPizzas($arrayPizzasVendidasFechas) . "\n";

    //ROMPE
    $arrayVentasUsuario = ventasUsuario($arrayVentas, $email);
    $mensaje .= 'Cantidad de pizzas vendidas al usuario ingresado: ' . "\n";
    $mensaje .= Pizza::ListarPizzas($arrayVentasUsuario) . "\n";

    $arrayVentasSabor = ventasSabor($arrayVentas, $sabor);
    $mensaje .= 'Cantidad de pizzas vendidas del sabor ingresado: ' . "\n";
    $mensaje .= Pizza::ListarPizzas($arrayVentasSabor) . "\n";

    $mensaje = trim($mensaje, "\n");
    echo $mensaje;
} else {
    throw new Error('Parametros de la petición no validos/incompletos', 400);
}

//-------------------Funciones-------------------
function cantidadPizzasVendidas($arrayVentas)
{
    $contador = 0;
    foreach ($arrayVentas as $v) {
        $contador += $v->getPizzaVendida()->getCantidad();
    }
    return $contador;
}

function ventasEntreFechas($arrayVentas, $inicio, $final)
{
    $arrayVentasFechas = [];
    $fechaInicio = NULL;
    $fechaFinal = NULL;
    $fechaHoy = date("d-m-y");
    !$inicio ? $fechaInicio = date("d-m-y", strtotime("-1 day")) : $fechaInicio = date($inicio);
    !$final ? $fechaFinal = $fechaHoy : $fechaFinal = date($final);

    foreach ($arrayVentas as $v) {
        if ($v->getFecha() >= $fechaInicio && $v->getFecha() <= $fechaFinal) {
            array_push($arrayVentasFechas, $v->getPizzaVendida());
        }
    }

    return $arrayVentasFechas;
}

function ventasUsuario($arrayVentas, $email)
{
    $arrayUsuarioVentas = [];
    foreach ($arrayVentas as $v) {
        if ($v->getEmail() == $email) {
            array_push($arrayUsuarioVentas, $v->getPizzaVendida());
        }
    }
    return $arrayUsuarioVentas;
}

function ventasSabor($arrayVentas, $sabor)
{
    $arraySaborVentas = [];
    foreach ($arrayVentas as $v) {
        $pizza = $v->getPizzaVendida();
        if ($pizza->getSabor() == $sabor) {
            array_push($arraySaborVentas, $pizza);
        }
    }
    return $arraySaborVentas;
}
