<?php
include_once "./Venta.php";
parse_str(file_get_contents("php://input"), $put_vars);

if (isset($put_vars["numeroPedido"]) && isset($put_vars["email"]) && isset($put_vars["sabor"]) && isset($put_vars["tipo"]) && isset($put_vars["cantidad"])) {
    $numeroPedido = $put_vars["numeroPedido"];
    $email = $put_vars["email"];
    $sabor = $put_vars["sabor"];
    $tipo = strtolower($put_vars["tipo"]);
    $cantidad = $put_vars["cantidad"];
    try {
        $arrayVentas = Venta::CargarVentaArrayJSON();
        $mensaje = ModificarVenta($arrayVentas, $numeroPedido, $email, $sabor, $tipo, $cantidad);
        echo $mensaje;
    } catch (Error $ex) {
        throw $ex;
    }
} else {
    throw new Error('Parametros de la petición no validos/incompletos.', 400);
}

function ModificarVenta($arrayVentas, $numeroPedido, $email, $sabor, $tipo, $cantidad)
{
    $mensaje = "No se encontro el pedido del usuario.";
    foreach ($arrayVentas as $k => $v) {
        if ($v->getNumeroPedido() == $numeroPedido && $v->getEmail() == $email) {
            $mensaje = "Se modifico el pedido del usuario.";
            $pizzaAux = $v->getPizzaVendida();
            $pizza = new stdClass;
            $pizza->id = $pizzaAux->getId();
            $pizza->sabor = $sabor;
            $pizza->precio = $pizzaAux->getPrecio();
            $pizza->tipo = $tipo;
            $pizza->cantidad = $cantidad;
            $arrayVentas[$k]->setPizzaVendida($pizza);
            Venta::GuardarVentaJSON($arrayVentas);
            break;
        }
    }
    return $mensaje;
}