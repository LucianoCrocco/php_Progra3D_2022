<?php

include_once "./pizza.php";

if (isset($_POST["sabor"]) && isset($_POST["precio"]) && isset($_POST["tipo"]) && isset($_POST["cantidad"]) && isset($_files["imagen"])) {
    $sabor = $_POST["sabor"];
    $tipo = strtolower($_POST["tipo"]);
    $precio = $_POST["precio"];
    $cantidad = $_POST["cantidad"];
    try {
        $pizza = new Pizza(null, $sabor, $precio, $tipo, $cantidad);
        $destino = "./ImagenesDePizzas/" . $tipo . '-' . $sabor;
        move_uploaded_file($_files["imagen"]["tmp_name"], $destino);
        Pizza::ManejadorAgregarPizza($pizza);
        echo 'Se cargo la pizza correctamente';
    } catch (Error $ex) {
        throw $ex;
    }
} else {
    throw new Error('Parametros de la petición no validos/incompletos.', 400);
}