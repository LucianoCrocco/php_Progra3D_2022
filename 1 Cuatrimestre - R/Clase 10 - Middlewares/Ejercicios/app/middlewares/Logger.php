<?php

use Slim\Psr7\Response;
class Logger
{
    /* Para que sirve este middleware que trae el proyecto si no aparece ni en la ppt.
    No se usa mas.
    public static function LogOperacion($request, $response, $next)
    {
        $retorno = $next($request, $response);
        return $retorno;
    }
    */


    public static function LogOperacion($request, $handler)
    {
        $requestType=$request->getMethod();
        $response = $handler->handle($request);
        $response->getBody()->write("Hola mundo, la peticion se hizo con ". $requestType);
        return $response;
    }

    public static function VerificadorDeCredenciales($request, $handler)
    {
        $requestType=$request->getMethod();
        //$response = $handler->handle($request); 
        $response = new Response(); 

        if($requestType == 'GET'){
            $response = $handler->handle($request); 
            $response->getBody()->write('Metodo: ' . $requestType . ". No verificar ");
        } else if($requestType == 'POST'){
            $response = $handler->handle($request); 
            $response->getBody()->write('Metodo: ' . $requestType . ". Verificar ");
            $dataParseada = $request->getParsedBody();
            $nombre = $dataParseada['usuario'];
            $perfil = $dataParseada['perfil'];
            if($perfil == 'admin'){
                $response->getBody()->write('Bienvenido ' . $nombre);
            } else {
                $response->getBody()->write('Usuario no autorizado!');
            }
        }
        return $response;
    }

    public static function VerificadorDeCredencialesJSON($request, $handler)
    {
        $requestType = $request->getMethod();
        $response = $handler->handle($request);
        $rtn = json_encode("API => GET");
        if($requestType == 'GET'){
            $response->setStatus(200);
            $response->getBody()->write($rtn);
        }
        return $response;
    }
}