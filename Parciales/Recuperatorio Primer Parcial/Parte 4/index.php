<?php
date_default_timezone_set('America/Argentina/Buenos_Aires');
try {
    switch ($_SERVER["REQUEST_METHOD"]) {
        case 'GET':
            if (explode("/", $_SERVER["REQUEST_URI"])[1] == 'ConsultaVentas') {
                include_once "./Controladores/ConsultasVentas.php";
            } else if ($_SERVER["REQUEST_URI"] == '/ConsultaDevoluciones') {
                include_once "./Controladores/ConsultasDevoluciones.php";
            }
            break;
        case 'POST':
            if ($_SERVER["REQUEST_URI"] == '/HamburgesaCarga') {
                include_once './Controladores/HamburgesaCarga.php';
            } else if ($_SERVER["REQUEST_URI"] == '/Consulta') {
                include_once "./Controladores/HamburgesaConsultar.php";
            } else if ($_SERVER["REQUEST_URI"] == '/Venta') {
                include_once './Controladores/AltaVenta.php';
            } else if ($_SERVER["REQUEST_URI"] == '/Devolucion') {
                include_once './Controladores/DevolverHamburgesa.php';
            }
            break;
        case 'PUT':
            include_once './Controladores/ModificarVenta.php';
            break;
        case 'DELETE':
            include_once "./Controladores/BorrarVenta.php";
            break;
    }
} catch (Error $ex) {
    print('Error: ' . $ex->getMessage() . ' Status code: ' . $ex->getCode() . ' Archivo y Linea: ' . $ex->getFile() . ' ' . $ex->getLine());
}