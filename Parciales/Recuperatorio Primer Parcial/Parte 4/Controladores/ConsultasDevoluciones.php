<?php
include_once "./Modelos/Devolucion.php";
include_once "./Modelos/Cupon.php";

$mensaje = '';

$mensaje .= 'Lista de Cupones dados:' . "\n";
$arrayCupones = Cupon::CargarCuponArrayJSON();
$mensaje .= Cupon::ListarCupones($arrayCupones);

$mensaje .= "\n" . 'Lista de Devoluciones:' . "\n";
$arrayDevolucion = Devolucion::CargarDevolucionArrayJSON();
$mensaje .= Devolucion::ListarDevoluciones($arrayDevolucion);

$mensaje .= "\n" . 'Lista de Devoluciones con sus cupones:' . "\n";
$mensaje .= DevolucionesConCupones($arrayDevolucion, $arrayCupones);

echo $mensaje;

function DevolucionesConCupones($arrayDevolucion, $arrayCupones)
{
    $mensaje = '';
    foreach ($arrayDevolucion as $d) {
        foreach ($arrayCupones as $c) {
            if ($d->idCuponDado == $c->id) {
                $mensaje .= "Id devolucion: " . $d->id . ", Razon Devolucion: " . $d->razonDevolucion . ", Numero del pedido devuelto: " . $d->numeroPedidoDevuelto . ", Id cupon dado: " . $c->id . ", Usado: " . ($c->usado ? "Si" : "No") . ".\n";
                break;
            }
        }
    }
    return $mensaje;
}