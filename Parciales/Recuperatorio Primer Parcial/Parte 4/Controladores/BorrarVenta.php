<?php
include_once "./Modelos/Venta.php";
parse_str(file_get_contents("php://input"), $put_vars);

if (isset($put_vars["numeroPedido"])) {
    $numeroPedido = $put_vars["numeroPedido"];
    try {
        $arrayVentas = Venta::CargarVentaArrayJSON();
        $mensaje = BorrarVenta($arrayVentas, $numeroPedido);
        echo $mensaje;
    } catch (Error $ex) {
        throw $ex;
    }
} else {
    throw new Error('Parametros de la petición no validos/incompletos.', 400);
}

function BorrarVenta($arrayVentas, $numeroPedido)
{
    $mensaje = "No se encontro el pedido del usuario.";
    foreach ($arrayVentas as $k => $v) {
        if ($v->numeroPedido == $numeroPedido) {
            $mensaje = "Se borro la Venta.";
            unset($arrayVentas[$k]);
            $nombre = $v->hamburgesaVendida->tipo . '-' . $v->hamburgesaVendida->nombre . "-" . explode('@', $v->email)[0] . "-" . $v->fecha;
            $destino = "./BACKUPVENTAS/" . $nombre;
            rename("./ImagenesDeLaVenta/" . $nombre, $destino);
            Venta::GuardarVentaJSON($arrayVentas);
            break;
        }
    }
    return $mensaje;
}