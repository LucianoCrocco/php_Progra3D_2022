<?php
class Cupon implements JsonSerializable
{
    public int $id;
    public string $emailPropietario;
    public bool $usado;
    public $fecha;
    public function __construct($id, $emailPropietario, $usado, $fecha)
    {
        $id ? $this->id = $id : $this->id = random_int(0, 10000);
        $this->emailPropietario = $emailPropietario;
        $usado ? $this->usado = $usado : $this->usado = false;
        $fecha ? $this->fecha = $fecha : $this->fecha = date("d-m-y H:i:s");
    }
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }
    public static function GuardarCuponesJSON($arrayCupones)
    {
        if (is_array($arrayCupones)) {
            $archivo = fopen("./Data/Cupones.json", "w");
            if ($archivo != FALSE) {
                $json = json_encode($arrayCupones);
                fputs($archivo, $json);
                fclose($archivo);
            }
        } else {
            throw new Error('El dato ingresado no es un array.', 500);
        }
    }
    public static function CargarCuponArrayJSON()
    {
        $array = [];
        if (file_exists("./Data/Cupones.json")) {
            $archivo = fopen("./Data/Cupones.json", "r");
            if ($archivo != FALSE) {
                $mensaje = fread($archivo, filesize("./Data/Cupones.json"));
                $arrayAux = json_decode($mensaje);
                $array = Cupon::GenerarArrayCupones($arrayAux);
                fclose($archivo);
            }
        }
        return $array;
    }
    private static function GenerarArrayCupones($array)
    {
        $arrayNew = [];
        foreach ($array as $v) {
            $cupon = new Cupon($v->id, $v->emailPropietario, $v->usado, $v->fecha);
            array_push($arrayNew, $cupon);
        }
        return $arrayNew;
    }
    public static function VerificarExistenciaCupon($idCupon)
    {
        $array = Cupon::CargarCuponArrayJSON();
        $flagExiste = false;
        foreach ($array as $v) {
            if ($v->id == $idCupon) {
                $flagExiste = true;
                break;
            }
        }
        return $flagExiste;
    }

    public static function MarcarCupon($idCupon)
    {
        $array = Cupon::CargarCuponArrayJSON();
        foreach ($array as $v) {
            if ($v->id == $idCupon) {
                $v->usado = true;
                break;
            }
        }
        Cupon::GuardarCuponesJSON($array);

    }

    public static function ListarCupones($array)
    {
        $mensaje = '';
        foreach ($array as $v) {
            $mensaje .= Cupon::ListarCupon($v);
        }
        return $mensaje;
    }
    private static function ListarCupon($cupon)
    {
        return 'Email propietario: ' . $cupon->emailPropietario . ', usado: ' . ($cupon->usado ? "Si" : "No ") . ".\n";
    }
}